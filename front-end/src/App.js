import React, { useState, useEffect } from 'react';
import NavbarView from './CustomComponents/NavbarView';
import HomeView from './CustomComponents/HomeView';
import LoginView from './CustomComponents/LoginView';
import RegisterView from './CustomComponents/RegisterView';
import { BrowserRouter, Routes, Route, useLocation } from 'react-router-dom';

function MainComponent() {
  const location = useLocation();
  const [navItem, setNavItem] = useState('');
  //const [logged, setLogged] = useState(false);
  
  const pathToId = {
    '/': 'home',
    '/login': 'login',
    '/myaccount': 'myaccount',
    '/getabook': 'getabook',
    '/contactus': 'contactus',
  };

  const handleLogin = () => {
    //setLogged(true);
    console.log("Logged in")
  }

  useEffect(() => {
    const id = pathToId[location.pathname];
    setNavItem(id);
  }, [location, pathToId]);

  return (
    <>
      <NavbarView itemName={navItem} />
      <Routes>
        <Route path="/" element={<HomeView />} />
        <Route path="/login" element={<LoginView onLogin={handleLogin} />} />
        <Route path="/register" element={<RegisterView />} />
      </Routes>
    </>
  );
}

export default function App() {
  return (
    <BrowserRouter>
      <MainComponent />
    </BrowserRouter>
  );
}