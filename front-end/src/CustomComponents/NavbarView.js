import React, { useState, useEffect }  from 'react';
import PropTypes from 'prop-types';
import { HomeOutlined, UserOutlined, BookOutlined, PhoneOutlined, LoginOutlined } from '@ant-design/icons';
import { Menu, ConfigProvider } from 'antd';
import "../Styles/NavbarViewStyle.css";
import LogoShort from "../Images/logoShort.png";
import {Link} from 'react-router-dom';

 const items = [
    {
      label: <Link to='/' style={{textDecoration: 'none'}}>Home</Link>,
      key: 'home',
      icon: <HomeOutlined />,
      style: {
        color: 'gray'
      }
    },

    {
      label: <Link to='/myaccount' style={{textDecoration: 'none'}}>My account</Link>,
      key: 'myaccount',
      icon: <UserOutlined />,
      style: {
        color: 'gray'
      }
    },

    {
      label: <Link to='/getabook' style={{textDecoration: 'none'}}>Get a book</Link>,
      key: 'getabook',
      icon: <BookOutlined />,
      style: {
        color: 'gray'
      },
    },

    {
      label: <Link to='/contact' style={{textDecoration: 'none'}}>Contact us</Link>,
      key: 'contactus',
      icon: <PhoneOutlined />,
      style: {
        color: 'gray'
    }
    },

    {
        label: <Link to='/login' style={{textDecoration: 'none'}}>Login</Link>,
        key: 'login',
        icon: <LoginOutlined />,
        style: {
            marginLeft: '1%',
            color: 'gray'
        }
      }
  ]; 

  const NavbarView = ({itemName}) => {
    const [current, setCurrent] = useState(itemName);
  
    // Update 'current' state whenever 'itemName' prop changes
    useEffect(() => {
      setCurrent(itemName);
    }, [itemName]);
  
    const onClick = (e) => {
      setCurrent(e.key);
    };
  

  return (
    <>
      <div className='mainNav'>
        <div className='navLogoText'>
          <img src={LogoShort} style={{width: '7vw', height: '4vw'}}/>
        </div>
        <ConfigProvider
          theme={{
            components: {
              Menu: {
                iconSize: '1.5vw',
                fontSize: '1.7vw'
              },
            },
          }}
        >
          <Menu onClick={onClick} selectedKeys={[current]} mode="horizontal" items={items} className='navbar-menu'/>
        </ConfigProvider>
      </div> 
    </>
  );
};

NavbarView.propTypes = {
    itemName: PropTypes.string.isRequired,
  };


export default NavbarView;
