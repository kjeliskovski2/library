import React, { useState } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import { Button, Checkbox, Form, Input } from 'antd';
import "../Styles/RegisterViewStyle.css";
import LogoShort from "../Images/logoShort.png";

const formItemLayout = {
    labelCol: {
        xs: {
            span: 24,
        },
        sm: {
            span: 8,
        },
    },
    wrapperCol: {
        xs: {
            span: 24,
        },
        sm: {
            span: 16,
        },
    },
};

const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0,
        },
        sm: {
            span: 16,
            offset: 8,
        },
    },
};

const RegisterView = () => {

    const [user, setUser] = useState({ name: '', surname: '', email: '', username: '', password: '' });
    const [agreed, setAgreed] = useState(false);

    const getTextFromInput = (e) => {
        setUser({
            ...user,
            [e.target.name]: e.target.value
        });
    }
    
    const acceptAgreement = () => {
        setAgreed(!agreed);
    }

    const registerPost = () => {
        axios.post(process.env.REACT_APP_URL+'/register', {
            name: user.name,
            surname: user.surname,
            email: user.email,
            username: user.username,
            password: user.password,
        })
            .then((response) => {
                if (response.status === 200) {
                    console.log("Registered")
                } else {
                    console.log("Not registered")
                }
            })

    }

    const [form] = Form.useForm();



    return (
        <>
            <div className='mainReg'>
                <img src={LogoShort} style={{ width: '10vw', height: '6vw' }} />
                <h5 style={{fontSize: '1.5vw'}}>Register your account</h5>
                <div className='formReg'>
                    <Form
                        {...formItemLayout}
                        form={form}
                        name="register"
                        style={{
                            maxWidth: 600,
                        }}
                        scrollToFirstError
                    >

                        <Form.Item
                            name="name"
                            label="Name"
                            rules={[
                                {
                                    type: 'text',
                                    style: {
                                        fontSize: '5vw'
                                    }
                                },
                                {
                                    required: true,
                                    message: 'Please input your name!',
                                },
                            ]}
                        >
                            <Input style={{ fontSize: '1vw' }} onChange={(e) => getTextFromInput(e)} name="name" />
                        </Form.Item>

                        <Form.Item
                            name="surname"
                            label="Surname"
                            rules={[
                                {
                                    type: 'text',
                                },
                                {
                                    required: true,
                                    message: 'Please input your surname!',
                                },
                            ]}
                        >
                            <Input style={{ fontSize: '1vw' }} onChange={(e) => getTextFromInput(e)} name="surname" />
                        </Form.Item>

                        <Form.Item
                            name="email"
                            label="E-mail"
                            rules={[
                                {
                                    type: 'email',
                                    message: 'The input is not valid E-mail!',
                                },
                                {
                                    required: true,
                                    message: 'Please input your E-mail!',
                                },
                            ]}
                        >
                            <Input style={{ fontSize: '1vw' }} onChange={(e) => getTextFromInput(e)} name="email" />
                        </Form.Item>

                        <Form.Item
                            name="username"
                            label="Username"
                            rules={[
                                {
                                    type: 'text',
                                },
                                {
                                    required: true,
                                    message: 'Please input your username!',
                                },
                            ]}
                        >
                            <Input style={{ fontSize: '1vw' }} onChange={(e) => getTextFromInput(e)} name="username" />
                        </Form.Item>

                        <Form.Item
                            name="password"
                            label="Password"
                            rules={[
                                {
                                    required: true,
                                    message: 'Please input your password!',
                                },
                            ]}
                            hasFeedback
                        >
                            <Input.Password style={{ fontSize: '1vw' }} onChange={(e) => getTextFromInput(e)} name="password" />
                        </Form.Item>

                        <Form.Item
                            name="confirm"
                            label="Confirm Password"
                            dependencies={['password']}
                            hasFeedback
                            rules={[
                                {
                                    required: true,
                                    message: 'Please confirm your password!',
                                },
                                ({ getFieldValue }) => ({
                                    validator(_, value) {
                                        if (!value || getFieldValue('password') === value) {
                                            return Promise.resolve();
                                        }
                                        return Promise.reject(new Error('The new password that you entered do not match!'));
                                    },
                                }),
                            ]}
                        >
                            <Input.Password style={{ fontSize: '1vw' }} />
                        </Form.Item>

                        <Form.Item
                            name="agreement"
                            valuePropName="checked"
                            rules={[
                                {
                                    validator: (_, value) =>
                                        value ? Promise.resolve() : Promise.reject(new Error('Should accept agreement')),
                                },
                            ]}
                            {...tailFormItemLayout}
                        >
                            <Checkbox onChange={acceptAgreement}>
                                I have read the <Link to="/agreement" target="_blank" rel="noopener noreferrer" >agreement</Link>
                            </Checkbox>
                        </Form.Item>
                        <Form.Item {...tailFormItemLayout}>
                            {agreed && (<Button type="primary" htmlType="submit" onClick={registerPost}>
                                Register
                            </Button>)}
                        </Form.Item>
                    </Form>
                </div>
            </div>
        </>
    );
};
export default RegisterView;