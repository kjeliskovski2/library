import React, { useState } from 'react';
import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';
import { LockOutlined, UserOutlined } from '@ant-design/icons';
import { Button, Checkbox, Form, Input } from 'antd';
import "../Styles/LoginViewStyle.css";
import Logo from '../Images/logo.jpg';
import axios from 'axios';

const LoginView = ({ onLogin }) => {

  const [user, setUser] = useState({ username: '', password: '', logged: false, incorrectPW: false});

  const getTextFromInput = (e) => {
    setUser({
      ...user,
      [e.target.name]: e.target.value
    });
  }


  const loginPost = () => {
    axios.post(process.env.REACT_APP_URL+'/login', {
      username: user.username,
      password: user.password
    })
      .then((response) => {
        if (response.status === 200) {
          setUser({...user, logged: true, incorrectPW: false});
          onLogin();
        } else {
          setUser({...user, logged: false, incorrectPW: true});
        }
      })

  }


  return (
    <>
      <div className='loginContainer'>
        <div>
          <img src={Logo} alt='logo' style={{ width: '30vw', height: '15vw', marginLeft: '0vw', marginTop: '-13vw' }} />
        </div>
        <div className='loginForm'>
          <h2 style={{ textAlign: 'center', fontSize: '2.5vw', marginTop: '1vw', marginBottom: '1vw' }}>Login into your account</h2>
          {user.incorrectPW && 
          (<h3 style={{ textAlign: 'center', fontSize: '1.8vw', marginBottom: '1vw', color: 'red' }}>Incorrect credentials</h3>)
          }
          <Form
            name="normal_login"
            className="login-form"
            initialValues={{
              remember: true,
            }}
          >
            <Form.Item
              name="username"
              rules={[
                {
                  required: true,
                  message: 'Please input your Username!',
                },
              ]}
            >
              <Input
                prefix={<UserOutlined className="site-form-item-icon" />}
                placeholder="Username"
                style={{ fontSize: '1.2vw' }}
                onChange={(e) => getTextFromInput(e)}
                name='username'
              />
            </Form.Item>
            <Form.Item
              name="password"
              rules={[
                {
                  required: true,
                  message: 'Please input your Password!',
                },
              ]}
            >
              <Input
                prefix={<LockOutlined className="site-form-item-icon" />}
                type="password"
                placeholder="Password"
                style={{ fontSize: '1.2vw' }}
                onChange={(e) => getTextFromInput(e)}
                name='password'
              />
            </Form.Item>
            <Form.Item>
              <Form.Item name="remember" valuePropName="checked" noStyle>
                <Checkbox style={{ fontSize: '0.9vw' }}>Remember me</Checkbox>
              </Form.Item>

              <a style={{ marginLeft: '17.5vw', fontSize: '0.9vw' }} className="login-form-forgot" href="">
                Forgot password
              </a>
            </Form.Item>

            <Form.Item>
              <Button type="primary"
                htmlType="submit" className="login-form-button"
                style={{ fontSize: '1vw', textAlign: 'center', width: '5vw', height: '2vw' }}
                onClick={loginPost}
              >
                Log in
              </Button>
              <Link to="/register" style={{ marginLeft: '21vw', fontSize: '0.9vw' }} href="">Register now!</Link>
            </Form.Item>
          </Form>
        </div>
      </div>
    </>
  );
};

LoginView.propTypes = {
  onLogin: PropTypes.func.isRequired,
};

export default LoginView;