import React from "react";
import { Carousel, Avatar, Card, Col, Row } from 'antd';
import "../Styles/HomeViewStyles.css"
import CarImg from "../Images/3303210.jpg"
import TogetherImg from "../Images/together.jpg"
import ReadingImg from "../Images/reading.jpg"
import Logo from "../Images/logo.jpg"
import LogoShort from "../Images/logoShort.png"

const { Meta } = Card;


export default function HomeView() {

    return (
        <>
            <div className="carouselHome">
                <Carousel autoplay autoplaySpeed={3000} effect="fade">
                    <div>
                        <img src={Logo} alt="book" style={{ marginLeft: '18vw', height: '32vw', width: '50vw', }} />
                    </div>
                    <div>
                        <img src={CarImg} alt="book" style={{ marginLeft: '18vw', height: '34vw', width: '50vw', }} />
                        <h2 style={{ textAlign: 'center', fontSize: '2.5vw', }}>Reading</h2>
                    </div>

                    <div>
                        <img src={TogetherImg} alt="book" style={{ marginLeft: '3vw', height: '36vw', width: '83vw', }} />
                        <h2 style={{ textAlign: 'center', fontSize: '2.5vw' }}>Gaining knowledge</h2>
                    </div>

                    <div>
                        <img src={ReadingImg} alt="book" style={{ marginLeft: '18vw', height: '34vw', width: '50vw', }} />
                        <h2 style={{ textAlign: 'center', fontSize: '2.5vw', }}>Sharing</h2>
                    </div>
                </Carousel>

            </div>
            <div className="content">
                <div style={{ width: '19vw' }}>
                    <Carousel autoplay autoplaySpeed={7000} arrows infinite={true}>
                        <div>
                            <Card
                                style={{
                                    width: '19vw',
                                    fontSize: '0.9vw',
                                }}
                                cover={
                                    <img
                                        alt="Yumeonai"
                                        src="https://i.imgur.com/nbKZld1.jpeg"
                                        style={{ width: '19.5vw', height: '28vw' }}
                                    />
                                }
                                actions={[
                                    <h5 style={{ fontSize: '1.5vw', fontWeight: 'bold' }} key='test'>Read more</h5>
                                ]}
                            >
                                <Meta
                                    avatar={<Avatar src="https://media.licdn.com/dms/image/C5603AQG3YDh29Y4EHA/profile-displayphoto-shrink_800_800/0/1645141559180?e=1720051200&v=beta&t=WfTZfIYuPye600rHM_4ctHT1v6mSNT8Ftt2hJGoLUTs" />}
                                    title={<div style={{ fontSize: '1vw' }}>Promoting young authors</div>}
                                    description="This month we are promoting young authors. For you we have the book `Yumeonai` by Damianos Nikolaou, Greece. This book will take you on a journey through the world of dreams and nightmares that will not leave you indiferrent. "
                                />
                            </Card>
                        </div>

                        <div>
                            <Card
                                style={{
                                    width: '18vw',
                                    fontSize: '0.9vw',
                                }}
                                cover={
                                    <img
                                        alt="NovumLibri"
                                        src={LogoShort}
                                        style={{ width: '18vw', height: '18vw' }}
                                    />
                                }
                                actions={[
                                    <a href="https://www.goodreads.com/list/show/196307.Best_Books_of_2024"
                                        target="_blank" rel="noopener noreferrer" key='link'
                                        style={{ textDecoration: 'none' }}
                                    >
                                        <h5 style={{ fontSize: '1.5vw', fontWeight: 'bold' }}>Read more</h5>
                                    </a>

                                ]}
                            >
                                <Meta
                                    avatar={<Avatar src="https://media.licdn.com/dms/image/C5603AQG3YDh29Y4EHA/profile-displayphoto-shrink_800_800/0/1645141559180?e=1720051200&v=beta&t=WfTZfIYuPye600rHM_4ctHT1v6mSNT8Ftt2hJGoLUTs" />}
                                    title={<div style={{ fontSize: '1vw' }}>NovumLibri is hiring</div>}
                                    description="We are looking for a new team member to join our team. If you are passionate about books and reading, this is the perfect opportunity for you. Apply now!"
                                />
                            </Card>
                        </div>
                    </Carousel>
                </div>

                <div style={{ width: '19vw' }}>
                    <Carousel autoplay autoplaySpeed={7000} arrows infinite={true}>
                        <div>
                            <Card
                                style={{
                                    width: '19vw',
                                    fontSize: '0.9vw',
                                }}
                                cover={
                                    <img
                                        alt="Harry Potter book"
                                        src="https://kids.scholastic.com/content/kids64/en/books/harry-potter/_jcr_content/root/responsivegrid/responsivegrid_1333535796/responsivegrid_19882/image_copy_1366789715.coreimg.100.1285.png/1693924413857/5-hp-grandpre-refresh-phoenix-sm.png"
                                        style={{ width: '19.5vw', height: '28vw' }}
                                    />
                                }
                                actions={[
                                    <h5 style={{ fontSize: '1.5vw', fontWeight: 'bold' }} key='test'>Read more</h5>
                                ]}
                            >
                                <Meta
                                    avatar={<Avatar src="https://media.licdn.com/dms/image/C5603AQG3YDh29Y4EHA/profile-displayphoto-shrink_800_800/0/1645141559180?e=1720051200&v=beta&t=WfTZfIYuPye600rHM_4ctHT1v6mSNT8Ftt2hJGoLUTs" />}
                                    title={<div style={{ fontSize: '1vw' }}>Harry Potter books giveaway</div>}
                                    description="We are organising a Harry Potter book giveaway. The first 1000 people to register will get a free copy of the first book in the series. Register now to get your copy."
                                />
                            </Card>
                        </div>

                        <div>
                            <Card
                                style={{
                                    width: '18vw',
                                    fontSize: '0.9vw',
                                }}
                                cover={
                                    <img
                                        alt="1984"
                                        src="https://book-website.com/wp-content/uploads/2023/10/nineteen-eighty-four-1984-george.jpg"
                                        style={{ width: '18vw', height: '25vw' }}
                                    />
                                }
                                actions={[
                                    <a href="https://www.goodreads.com/list/show/196307.Best_Books_of_2024"
                                        target="_blank" rel="noopener noreferrer" key='link'
                                        style={{ textDecoration: 'none' }}
                                    >
                                        <h5 style={{ fontSize: '1.5vw', fontWeight: 'bold' }}>Read more</h5>
                                    </a>

                                ]}
                            >
                                <Meta
                                    avatar={<Avatar src="https://media.licdn.com/dms/image/C5603AQG3YDh29Y4EHA/profile-displayphoto-shrink_800_800/0/1645141559180?e=1720051200&v=beta&t=WfTZfIYuPye600rHM_4ctHT1v6mSNT8Ftt2hJGoLUTs" />}
                                    title={<div style={{ fontSize: '1vw' }}>Best books of 2024</div>}
                                    description="We have compiled a list of the best books of 2024. The list includes books from various genres and authors. Check out the list to find your next read."
                                />
                            </Card>
                        </div>
                    </Carousel>
                </div>

                <div style={{ width: '18vw' }}>
                    <Carousel autoplay autoplaySpeed={7000} arrows infinite={true}>
                        <div>
                            <Card
                                style={{
                                    width: '17vw',
                                    fontSize: '0.9vw',
                                    height: 'auto'
                                }}
                                cover={
                                    <img
                                        alt="George Orwell"
                                        src="https://upload.wikimedia.org/wikipedia/commons/8/82/George_Orwell%2C_c._1940_%2841928180381%29.jpg"
                                        style={{ width: '17vw', height: '25vw' }}
                                    />
                                }
                                actions={[
                                    <a href="https://iep.utm.edu/george-orwell/"
                                        target="_blank" rel="noopener noreferrer" key='link'
                                        style={{ textDecoration: 'none' }}
                                    >
                                        <h5 style={{ fontSize: '1.5vw', fontWeight: 'bold' }}>Read more</h5>
                                    </a>
                                ]}
                            >
                                <Meta
                                    avatar={<Avatar src="https://media.licdn.com/dms/image/C5603AQG3YDh29Y4EHA/profile-displayphoto-shrink_800_800/0/1645141559180?e=1720051200&v=beta&t=WfTZfIYuPye600rHM_4ctHT1v6mSNT8Ftt2hJGoLUTs" />}
                                    title={<div style={{ fontSize: '1vw' }}>Author of the month</div>}
                                    description="This months autor is George Orwell. He was an English novelist, essayist, journalist and critic, whose work is marked by lucid prose, awareness of social injustice, opposition to totalitarianism, and outspoken support of democratic socialism."

                                />
                            </Card>
                        </div>

                        <div>
                            <Card
                                style={{
                                    width: '18vw',
                                    fontSize: '0.9vw',
                                }}
                                cover={
                                    <img
                                        alt="World book day"
                                        src="https://img.freepik.com/free-vector/organic-flat-world-book-day-illustration_23-2148879847.jpg?t=st=1714655999~exp=1714659599~hmac=d735501ae15846507ad140096541d6d6b43aaacaf7880692de95c3f9870ba237&w=740"
                                        style={{ width: '18vw', height: '25vw' }}
                                    />
                                }
                                actions={[
                                    <h5 style={{ fontSize: '1.5vw', fontWeight: 'bold' }} key='test'>Read more</h5>
                                ]}
                            >
                                <Meta
                                    avatar={<Avatar src="https://media.licdn.com/dms/image/C5603AQG3YDh29Y4EHA/profile-displayphoto-shrink_800_800/0/1645141559180?e=1720051200&v=beta&t=WfTZfIYuPye600rHM_4ctHT1v6mSNT8Ftt2hJGoLUTs" />}
                                    title={<div style={{ fontSize: '1vw' }}>Celebrate world book day</div>}
                                    description="The theme for World Book Day 2024 is `Read Your Way.` Explaining this year's theme, the charity said: `World Book Day 2024 will celebrate that children are more likely to enjoy reading when their choices are championed and we make reading fun.` "
                                />
                            </Card>
                        </div>
                    </Carousel>
                </div>

            </div>

            <div className="comments">
                <div style={{ width: '90vw', display: 'flex', justifyContent: 'space-evenly', alignItems: 'center', flexWrap: 'wrap' }}>
                    <Row gutter={40}>
                        <Col span={10}>
                            <Card title="User 1" bordered={false}>

                                This bookshop is amazing. I found all the books I was looking for and the staff was very helpful. I will definitely come back.
                            </Card>
                        </Col>
                        <Col span={7}>
                            <Card title="User 2" bordered={false}>
                                I love reading and this is my favorite place to get books. The selection is great and the prices are very good.
                            </Card>
                        </Col>
                        <Col span={7}>
                            <Card title="User 3" bordered={false}>
                                I am very happy with the service I received. The staff was very friendly and helped me find the perfect book for my needs.
                            </Card>
                        </Col>
                    </Row>
                </div>

            </div>
        </>
    )
}