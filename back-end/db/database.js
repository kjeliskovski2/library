require('dotenv').config();
const { MongoClient, ServerApiVersion } = require('mongodb');
const uri = process.env.DB_URI;


const client = new MongoClient(uri, {
  serverApi: {
    version: ServerApiVersion.v1,
    strict: true,
    deprecationErrors: true,
  }
});

async function connectToMongoDB() {
  try {
    await client.connect();
    await client.db("admin").command({ ping: 1 });
    console.log("Database connected!");
  } catch (error) {
    console.error('Error connecting to MongoDB:', error);
  }
}

const database = client.db("LibraryDB");

async function regUser(name, surname, email, username, password) {
  try {
    await database.collection("User").insertOne({
      name: name,
      surname: surname,
      email: email,
      username: username,
      password: password,
    });
  
  } catch (error) {
    console.error('DB: Error inserting user:', error);
  }
}

async function authUser(username) {
  const collection = database.collection("User");

  try {
    const result = await collection.findOne({ username: username });

    if (result) {
      return result.password
    } else {
      return null;
    }

  } catch (e) {
    console.log("DB: Error authenticating user:", e);
    return null;
  } 

}

module.exports = { connectToMongoDB, 
    regUser,
    authUser, 
    client 

};
