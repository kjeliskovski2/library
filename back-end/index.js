const express              = require('express');
const cors                 = require('cors');
const app                  = express();
const register             = require('./routes/register');
const login                = require('./routes/login');
const bodyParser           = require('body-parser');
const { connectToMongoDB } = require("./db/database");
const port                 = 9999;

connectToMongoDB().catch(console.dir);

app.use(bodyParser.json());
app.use(express.urlencoded({extended : true}));
app.use(cors({
  methods:["GET", "POST"],
}))
app.use("/register", register);
app.use("/login", login);

app.get("/", (req, res) => {
  res.send("Hello");
});

app.listen(port, () => {
  console.log("Server is running on port", port);
});
