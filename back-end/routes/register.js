const express        = require('express');
const bodyParser     = require('body-parser');
const register       = express.Router();
const { regUser }    = require('../db/database');

register.use(bodyParser.json());

register.post("/", async (req, res) => {
    var name     = req.body.name;
    var surname  = req.body.surname;
    var email    = req.body.email;
    var username = req.body.username;
    var password = req.body.password;

    if(name && surname && email && username && password) {
        await regUser(name, surname, email, username, password);
        res.sendStatus(200);
    } else {
        res. sendStatus(204)
        console.log("Register data missing!");
    }
    

})

module.exports = register;