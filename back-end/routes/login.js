const express      = require('express');
const login        = express.Router();
const bodyParser   = require('body-parser');
const { authUser } = require('../db/database');

login.use(bodyParser.json());

login.post("/", async (req, res) => {
    try{
        const username = req.body.username;
        const password = req.body.password;

        if (username && password){
            const result = await authUser(username, res);
            if((result === password) && (result != null)) {
                res.sendStatus(200)
            } else {
                res.sendStatus(204);
                console.log("Incorrect password");
            }
        } else {
        res.sendStatus(204)
        console.log("Please enter email and password!")
        }
        res.end();
   }catch(err){
    console.log(err)
    res.sendStatus(500)
   }
})

module.exports = login;