# NovumLibri - Online library

STILL IN DEVELOPMENT

## Project Description

This is a project about an online library called "NovumLibri". The information system is intended for multiple purposes out of which the main one is to allow the users to rent or buy new books. The system also serves as an information platform since it uploads useful information about the world of books.

## Functionalities

### Login

- Users can securely log in to their accounts using their credentials.
- Authentication is implemented to ensure the security of user accounts.

### Register

- New users can create accounts by providing necessary information such as username, email, and password.
- Account registration includes validation checks to ensure data integrity and security.

### Password Reset

- Users can request a password reset if they forget their current password.
- A password reset link is sent to the user's registered email address for verification and security.

### Renting a Book

- Users can browse the library catalog and select books they wish to rent.
- Renting functionality allows users to borrow books for a specified duration.

### Buying a Book

- Users can purchase books from the library's collection.
- Buying functionality includes secure payment processing and order confirmation.

### Account Management

- Users can manage their account settings, including profile information and password updates.
- Account management features ensure users have control over their personal data and security settings.


### API Integration

- Backend API implemented using Express.js to handle CRUD operations.
- Axios used for making HTTP requests between the frontend and backend.

## System Architecture

The system architecture is based on a client-server model, with the frontend implemented using React.js and the backend using Express.js. MongoDB is used as the database to store user information, book data, and transaction records. The frontend communicates with the backend API to fetch and update data, ensuring a seamless user experience.


## Visuals

<img src=" https://i.imgur.com/YgBm9px.png" width="1000px" height="600px" alt="login page" />

Check out this link for a video of the home page: https://i.imgur.com/7usV9ll.mp4
